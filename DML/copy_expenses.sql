COPY raw_expenses (expense_date, category, amount, currency) FROM 's3://bucket/expenses.csv'
    CREDENTIALS 'aws_access_key_id=access_key;aws_secret_access_key=secret_key'
    CSV DELIMITER ',' IGNOREHEADER 1 DATEFORMAT AS 'YYYY-MM-DD';
