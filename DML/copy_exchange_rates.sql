COPY raw_exchange_rates (Datum, AUD, BGN, BRL, CAD, CHF, CNY, DKK, EUR, GBP, HKD, HRK, HUF, IDR, ILS, INR, ISK,
                               JPY, KRW, MXN, MYR, NOK, NZD,
                               PHP, PLN, RON, RUB, SEK, SGD, THB, TRY, USD, XDR,
                               ZAR) FROM 's3://bucket/exchange_rates.csv'
    CREDENTIALS 'aws_access_key_id=access_key;aws_secret_access_key=secret_key'
    CSV DELIMITER '|' IGNOREHEADER 1 DATEFORMAT AS 'DD.MM.YYYY';
