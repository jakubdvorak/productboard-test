with exchange_rates as (
    select COALESCE(datum, date_actual) as datum,
           COALESCE(aud, LAST_VALUE(aud) IGNORE NULLS OVER (
               ORDER BY date_actual
               ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
               ))                       as aud,
           COALESCE(bgn, LAST_VALUE(bgn) IGNORE NULLS OVER (
               ORDER BY date_actual
               ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
               ))                       as bgn,
           COALESCE(brl, LAST_VALUE(brl) IGNORE NULLS OVER (
               ORDER BY date_actual
               ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
               ))                       as brl,
           COALESCE(cad, LAST_VALUE(cad) IGNORE NULLS OVER (
               ORDER BY date_actual
               ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
               ))                       as cad,
           COALESCE(chf, LAST_VALUE(chf) IGNORE NULLS OVER (
               ORDER BY date_actual
               ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
               ))                       as chf,
           COALESCE(cny, LAST_VALUE(cny) IGNORE NULLS OVER (
               ORDER BY date_actual
               ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
               ))                       as cny,
           COALESCE(dkk, LAST_VALUE(dkk) IGNORE NULLS OVER (
               ORDER BY date_actual
               ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
               ))                       as dkk,
           COALESCE(eur, LAST_VALUE(eur) IGNORE NULLS OVER (
               ORDER BY date_actual
               ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
               ))
                                        as eur,
           COALESCE(gbp, LAST_VALUE(gbp) IGNORE NULLS OVER (
               ORDER BY date_actual
               ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
               ))
                                        as gbp,
           COALESCE(hkd, LAST_VALUE(hkd) IGNORE NULLS OVER (
               ORDER BY date_actual
               ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
               ))
                                        as hkd,
           COALESCE(hrk, LAST_VALUE(hrk) IGNORE NULLS OVER (
               ORDER BY date_actual
               ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
               ))
                                        as hrk,
           COALESCE(huf, LAST_VALUE(huf) IGNORE NULLS OVER (
               ORDER BY date_actual
               ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
               ))
                                        as huf,
           COALESCE(idr, LAST_VALUE(idr) IGNORE NULLS OVER (
               ORDER BY date_actual
               ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
               ))
                                        as idr,
           COALESCE(ils, LAST_VALUE(ils) IGNORE NULLS OVER (
               ORDER BY date_actual
               ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
               ))
                                        as ils,
           COALESCE(inr, LAST_VALUE(inr) IGNORE NULLS OVER (
               ORDER BY date_actual
               ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
               ))
                                        as inr,
           COALESCE(isk, LAST_VALUE(isk) IGNORE NULLS OVER (
               ORDER BY date_actual
               ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
               ))
                                        as isk,
           COALESCE(jpy, LAST_VALUE(jpy) IGNORE NULLS OVER (
               ORDER BY date_actual
               ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
               ))
                                        as jpy,
           COALESCE(krw, LAST_VALUE(krw) IGNORE NULLS OVER (
               ORDER BY date_actual
               ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
               ))
                                        as krw,
           COALESCE(mxn, LAST_VALUE(mxn) IGNORE NULLS OVER (
               ORDER BY date_actual
               ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
               ))
                                        as mxn,
           COALESCE(myr, LAST_VALUE(myr) IGNORE NULLS OVER (
               ORDER BY date_actual
               ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
               ))
                                        as myr,
           COALESCE(nok, LAST_VALUE(nok) IGNORE NULLS OVER (
               ORDER BY date_actual
               ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
               ))
                                        as nok,
           COALESCE(nzd, LAST_VALUE(nzd) IGNORE NULLS OVER (
               ORDER BY date_actual
               ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
               ))
                                        as nzd,
           COALESCE(php, LAST_VALUE(php) IGNORE NULLS OVER (
               ORDER BY date_actual
               ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
               ))
                                        as php,
           COALESCE(pln, LAST_VALUE(pln) IGNORE NULLS OVER (
               ORDER BY date_actual
               ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
               ))
                                        as pln,
           COALESCE(ron, LAST_VALUE(ron) IGNORE NULLS OVER (
               ORDER BY date_actual
               ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
               ))
                                        as ron,
           COALESCE(rub, LAST_VALUE(rub) IGNORE NULLS OVER (
               ORDER BY date_actual
               ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
               ))
                                        as rub,
           COALESCE(sek, LAST_VALUE(sek) IGNORE NULLS OVER (
               ORDER BY date_actual
               ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
               ))
                                        as sek,
           COALESCE(sgd, LAST_VALUE(sgd) IGNORE NULLS OVER (
               ORDER BY date_actual
               ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
               ))
                                        as sgd,
           COALESCE(thb, LAST_VALUE(thb) IGNORE NULLS OVER (
               ORDER BY date_actual
               ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
               ))
                                        as thb,
           COALESCE(try, LAST_VALUE(try) IGNORE NULLS OVER (
               ORDER BY date_actual
               ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
               ))
                                        as try,
           COALESCE(usd, LAST_VALUE(usd) IGNORE NULLS OVER (
               ORDER BY date_actual
               ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
               ))
                                        as usd,
           COALESCE(xdr, LAST_VALUE(xdr) IGNORE NULLS OVER (
               ORDER BY date_actual
               ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
               ))
                                        as xdr,
           COALESCE(zar, LAST_VALUE(zar) IGNORE NULLS OVER (
               ORDER BY date_actual
               ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
               ))
                                        as zar


    from raw_exchange_rates
             full outer join date_dimension on raw_exchange_rates.datum = date_dimension.date_actual
    where date_dimension.date_actual >= to_date('2018-12-31', 'YYYY-MM-DD')
      AND date_dimension.date_actual <= to_date(getdate(), 'YYYY-MM-DD')),

     final_rates as (
         select datum,
                'AUD'    as currency,
                aud      as rate,
                1        as unit,
                'cnb.cz' as source

         from exchange_rates

         UNION ALL

         select datum,
                'BGN'    as currency,
                bgn      as rate,
                1        as unit,
                'cnb.cz' as source

         from exchange_rates

         UNION ALL

         select datum,
                'BRL'    as currency,
                brl      as rate,
                1        as unit,
                'cnb.cz' as source

         from exchange_rates

         UNION ALL

         select datum,
                'CAD'    as currency,
                cad      as rate,
                1        as unit,
                'cnb.cz' as source

         from exchange_rates

         UNION ALL

         select datum,
                'CHF'    as currency,
                chf      as rate,
                1        as unit,
                'cnb.cz' as source

         from exchange_rates

         UNION ALL

         select datum,
                'CNY'    as currency,
                cny      as rate,
                1        as unit,
                'cnb.cz' as source

         from exchange_rates

         UNION ALL

         select datum,
                'DKK'    as currency,
                dkk      as rate,
                1        as unit,
                'cnb.cz' as source

         from exchange_rates

         UNION ALL

         select datum,
                'EUR'    as currency,
                eur      as rate,
                1        as unit,
                'cnb.cz' as source

         from exchange_rates

         UNION ALL

         select datum,
                'GBP'    as currency,
                gbp      as rate,
                1        as unit,
                'cnb.cz' as source

         from exchange_rates

         UNION ALL

         select datum,
                'HKD'    as currency,
                hkd      as rate,
                1        as unit,
                'cnb.cz' as source

         from exchange_rates

         UNION ALL

         select datum,
                'HRK'    as currency,
                hrk      as rate,
                1        as unit,
                'cnb.cz' as source

         from exchange_rates

         UNION ALL

         select datum,
                'HUF'    as currency,
                huf      as rate,
                100      as unit,
                'cnb.cz' as source

         from exchange_rates

         UNION ALL

         select datum,
                'IDR'    as currency,
                idr      as rate,
                1000     as unit,
                'cnb.cz' as source

         from exchange_rates

         UNION ALL

         select datum,
                'ILS'    as currency,
                ils      as rate,
                1        as unit,
                'cnb.cz' as source

         from exchange_rates

         UNION ALL

         select datum,
                'INR'    as currency,
                inr      as rate,
                100      as unit,
                'cnb.cz' as source

         from exchange_rates

         UNION ALL

         select datum,
                'ISK'    as currency,
                isk      as rate,
                100      as unit,
                'cnb.cz' as source

         from exchange_rates

         UNION ALL

         select datum,
                'JPY'    as currency,
                jpy      as rate,
                100      as unit,
                'cnb.cz' as source

         from exchange_rates

         UNION ALL

         select datum,
                'KRW'    as currency,
                krw      as rate,
                100      as unit,
                'cnb.cz' as source

         from exchange_rates

         UNION ALL

         select datum,
                'MXN'    as currency,
                mxn      as rate,
                1        as unit,
                'cnb.cz' as source

         from exchange_rates

         UNION ALL

         select datum,
                'MYR'    as currency,
                MYR      as rate,
                1        as unit,
                'cnb.cz' as source

         from exchange_rates

         UNION ALL

         select datum,
                'NOK'    as currency,
                nok      as rate,
                1        as unit,
                'cnb.cz' as source

         from exchange_rates

         UNION ALL

         select datum,
                'NZD'    as currency,
                nzd      as rate,
                1        as unit,
                'cnb.cz' as source

         from exchange_rates

         UNION ALL

         select datum,
                'PHP'    as currency,
                php      as rate,
                100      as unit,
                'cnb.cz' as source

         from exchange_rates

         UNION ALL

         select datum,
                'PLN'    as currency,
                pln      as rate,
                1        as unit,
                'cnb.cz' as source

         from exchange_rates

         UNION ALL

         select datum,
                'RON'    as currency,
                ron      as rate,
                1        as unit,
                'cnb.cz' as source

         from exchange_rates

         UNION ALL

         select datum,
                'RUB'    as currency,
                rub      as rate,
                100      as unit,
                'cnb.cz' as source

         from exchange_rates

         UNION ALL

         select datum,
                'SEK'    as currency,
                sek      as rate,
                1        as unit,
                'cnb.cz' as source

         from exchange_rates

         UNION ALL

         select datum,
                'SGD'    as currency,
                sgd      as rate,
                1        as unit,
                'cnb.cz' as source

         from exchange_rates

         UNION ALL

         select datum,
                'THB'    as currency,
                thb      as rate,
                100      as unit,
                'cnb.cz' as source

         from exchange_rates
         UNION ALL

         select datum,
                'TRY'    as currency,
                try      as rate,
                1        as unit,
                'cnb.cz' as source

         from exchange_rates

         UNION ALL

         select datum,
                'USD'    as currency,
                usd      as rate,
                1        as unit,
                'cnb.cz' as source

         from exchange_rates
         UNION ALL

         select datum,
                'XDR'    as currency,
                xdr      as rate,
                1        as unit,
                'cnb.cz' as source

         from exchange_rates

         UNION ALL

         select datum,
                'ZAR'    as currency,
                zar      as rate,
                1        as unit,
                'cnb.cz' as source

         from exchange_rates)
select datum,
       currency,
       rate,
       unit::int,
       source
from final_rates
