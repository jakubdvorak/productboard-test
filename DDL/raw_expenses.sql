CREATE TABLE raw_expenses
(
    expense_date date           not null,
    category     varchar(256),
    amount       numeric(18, 4) not null,
    currency     varchar(3)     not null,
	loadtm       timestamp default timezone(('utc'::character varying)::text, now())
)
