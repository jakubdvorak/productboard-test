import time
import os
from urllib.request import urlopen

import pandas as pd
import boto3
from botocore.exceptions import NoCredentialsError

'''
    This obviously not ideal nor secure. Environmental variable would be better
'''

ACCESS_KEY = 'XXXXXXXXXXXXXXXXXXXXX'
SECRET_KEY = 'XXXXXXXXXXXXXXXXXXXXX'
S3_BUCKET = 'XXXXXXXXXXXXXXXXXXXXX'

def get_cnb_rates(url):
    try:
        print('Retrieving exchange rates from: {}'.format(url))
        return urlopen(url)
    except:
        print('Error in retrieving exchange rates.')

def get_csv(data):
    try:
        print('Saving into CSV file')
        current_time = time.strftime("%Y%m%d%H%M%S")
        file_name = 'exchange_rates_' + current_time + '.csv'
        file_path = os.path.dirname(os.path.abspath(__file__)) + '/' + file_name

        pd.read_csv(data, sep = '|',decimal=',').to_csv(file_name, sep = '|', index = False)

        print(file_path)
        print('Successfully saved')
        return file_path, file_name
    except:
        print('Error in saving CSV file')

def upload_to_aws(local_file, bucket, s3_file):
    s3 = boto3.client('s3', aws_access_key_id=ACCESS_KEY,
                      aws_secret_access_key=SECRET_KEY)

    try:
        s3.upload_file(local_file, bucket, s3_file)
        print("Upload Successful")
        return True
    except FileNotFoundError:
        print("The file was not found")
        return False
    except NoCredentialsError:
        print("Credentials not available")
        return False


def main():
    # Retrieving current year
    year = time.strftime("%Y")

    # Formatting URL
    url="http://www.cnb.cz/cs/financni_trhy/devizovy_trh/kurzy_devizoveho_trhu/rok.txt?rok={}".format(year)

    file, file_name = get_csv(get_cnb_rates(url))
    upload_to_aws(file, S3_BUCKET, file_name)

if __name__ == '__main__':
    main()
